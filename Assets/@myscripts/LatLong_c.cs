﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LatLong_c : MonoBehaviour {

	public Transform marker; // marker object
	public float radius = 5; // globe ball radius (unity units)
	public float latitude = 0f; // lat
	public float longitude = 0f; // long

	// Use this for initialization
	void Start () {

		latitude = Mathf.PI * latitude / 180;
		longitude = Mathf.PI * longitude / 180;

		latitude -= 1.570795765134f; // subtract 90 degrees (in radians)

		// and switch z and y (since z is forward)
		float xPos = (radius) * Mathf.Sin(latitude) * Mathf.Cos(longitude);
		float zPos = (radius) * Mathf.Sin(latitude) * Mathf.Sin(longitude);
		float yPos = (radius) * Mathf.Cos(latitude);

		marker.position = new Vector3(xPos, yPos, zPos);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
